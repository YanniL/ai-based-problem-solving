import java.util.*;
import java.io.*;

public class Breadth{
	final int MAX=100;
	FlightInfo flights[]=
	new FlightInfo[MAX];
	int numFlights=0;
	Stack btStack=new Stack();

	public static void main(String args[]){
		String to, from;
		Breadth ob = new Breadth();
		BufferedReader br = new
		BufferedReader(new InputStreamReader(System.in));
		ob.setup();
		try {
			System.out.print("From? ");
			from = br.readLine();
			System.out.print("To? ");
			to = br.readLine();
			ob.isFlight(from, to);
		if(ob.btStack.size() != 0)
			ob.route(to);
		} catch (IOException exc) {
		System.out.println("Error on input.");
		}
	}
	
	void setup(){
		addFlight("New York", "Chicago", 900);
		addFlight("Chicago", "Denver", 1000);
		addFlight("New York", "Toronto", 500);
		addFlight("New York", "Denver", 1800);
		addFlight("Toronto", "Calgary", 1700);
		addFlight("Toronto", "Los Angeles", 2500);
		addFlight("Toronto", "Chicago", 500);
		addFlight("Denver", "Urbana", 1000);
		addFlight("Denver", "Houston", 1000);
		addFlight("Houston", "Los Angeles", 1500);
		addFlight("Denver", "Los Angeles", 1000);
	}

	void route(String to){
		Stack rev=new Stack();
		int dist=0;
		FlightInfo f;
		int num = btStack.size();
		
		//Reverse the stack to display route.
		for(int i=0; i < num; i++){
			rev.push(btStack.pop());
		}

		for(int i=0; i< num; i++ ){
			f= (FlightInfo)rev.pop();
			System.out.print(f.from+ " to ");
			dist += f.distance;
		}
		System.out.println(to);
		System.out.println("Distance is "+ dist);
	} 

	void addFlight(String from, String to, int dist){
		if(numFlights < MAX){
			flights[numFlights] =  
			new FlightInfo(from, to, dist);
			numFlights++;
		}
		else System.out.println("Flight database full. \n");
	}

	int match(String from, String to){
		for(int i = numFlights-1; i > -1; i--){
			if(flights[i].from.equals(from) &&
			flights[i].to.equals(to) && !flights[i].skip){
				flights[i].skip=true;
				return flights[i].distance;
			}
		}
		return 0;
	}
	FlightInfo find(String from){
		for(int i=0; i<numFlights; i++){
			if(flights[i].from.equals(from) &&
			!flights[i].skip){
			FlightInfo f=new FlightInfo(flights[i].from,
				flights[i].to, flights[i].distance);
			flights[i].skip=true;
			return f;
			}
		}
		return null;
	}

	void isFlight(String from, String to){
		int dist, dist2;
		FlightInfo f;
		Stack resetStack=new Stack();

		//see if at destination.
		dist=match(from, to);
		if(dist != 0){
			btStack.push(new FlightInfo(from, to, dist));
			return;
		}

		while((f=find(from))!=null){
			resetStack.push(f);
			if( (dist=match(f.to, to))!=0 ){
				resetStack.push(f.to);
				btStack.push(new FlightInfo(from, f.to, 
						f.distance));
				btStack.push(new FlightInfo(f.to, to, dist));
				return;
			}
		}
		int i=resetStack.size();
		for(; i!=0; i--)
			resetSkip((FlightInfo)resetStack.pop());

			//Try another connection.
			f=find(from);
		if(f!=null){
			btStack.push(new FlightInfo(from,to,
			f.distance));
			isFlight(f.to, to);
		}
		else if(btStack.size() > 0){
			f=(FlightInfo)btStack.pop();
			isFlight(f.from, f.to);
		}	
	}

	void resetSkip(FlightInfo f){
		for(int i=0; i<numFlights; i++ ){
			if(flights[i].from.equals(f.from) && 
			flights[i].to.equals(f.to))
				flights[i].skip=false;
		}
	}

}











